import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column,Integer,String,Boolean,ForeignKey
from sqlalchemy.orm import backref
from sqlalchemy.orm import sessionmaker, relationship

base = declarative_base()

class User(base):
    __tablename__ = 'userinfo'

    userid = Column(Integer, primary_key = True, autoincrement=True)
    useremail = Column(String,unique = True)
    fleets = relationship("Fleet", secondary="userfleet")

class Fleet(base):
    __tablename__ = 'fleet'

    fleetid = Column(Integer, primary_key = True, autoincrement=True)
    fleetname = Column(String, unique = True)
    createdby = Column(Integer, ForeignKey("userinfo.userid"), nullable=False)
    users = relationship("User", secondary="userfleet")
    devices = relationship("Device", secondary = "devicefleet")

class Device(base):
    __tablename__='device'

    deviceid = Column(Integer, primary_key = True,autoincrement=True)
    serial = Column(String, unique = True)
    fleets = relationship("Fleet", secondary = "devicefleet")


class DeviceFleet(base):
    __tablename__ = 'devicefleet'

    devicefleetid = Column(Integer, primary_key = True,autoincrement=True)
    deviceid = Column(Integer, ForeignKey("device.deviceid", onupdate="CASCADE", ondelete="CASCADE"),nullable=False)
    fleetid = Column(Integer, ForeignKey("fleet.fleetid", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    invitedby = Column(Integer, ForeignKey("userinfo.userid", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    fleet = relationship ("Fleet", backref= "devicefleet")
    device = relationship("Device",backref = "devicefleet")
    user = relationship("User",backref ="devicefleet")


class UserFleet(base):
    __tablename__ = 'userfleet'

    userfleetid = Column(Integer, primary_key = True,autoincrement=True)
    userid = Column(Integer, ForeignKey("userinfo.userid",onupdate="CASCADE", ondelete="CASCADE"),nullable=False)
    fleetid = Column(Integer, ForeignKey("fleet.fleetid",onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    caninviteusers = Column(Boolean, server_default='f', nullable=False)
    caninvitedevices = Column(Boolean, server_default='f', nullable=False)
    canremovedevices = Column(Boolean, server_default='f', nullable=False)
    canremoveusers = Column(Boolean, server_default='f', nullable=False)
    user = relationship("User", backref=backref("userfleet"))
    fleet = relationship("Fleet", backref=backref("userfleet"))


def database_creation(base):
    DATABASE_URI = 'sqlite:///usersdb'

    db = sqlalchemy.create_engine(DATABASE_URI)

    Session = sessionmaker(db)
    session = Session()

    base.metadata.create_all(db)
    session.commit()
    session.close()
database_creation(base)

