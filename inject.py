from sqlalchemy import create_engine
import sqlalchemy
import json
from sqlalchemy.orm import Session
from marshmallow import Schema, fields
from sqlalchemy.exc import SQLAlchemyError

from Database import User,Fleet,UserFleet
from DatabaseSchema import UserFleetSchema
# Create a connection credentials to the PostgreSQL database

DATABASE_URI = 'postgres+psycopg2://postgres:Sednatech@sednadb-cluster.cluster-cqs5g5eukpuv.us-east-1.rds.amazonaws.com/usersdb'


# def lambda_handler(event, context):

#     print(event)
#     flagaws=True
#     if 'body-json' not in event:
#         flagaws=False        
        

    
#     if flagaws:
#         fleet_name = event['body-json']['fleetname']
#         user_email = event['context']['email']

   
fleet_name="fleet1"
user_email="aakash@sednatech.io"
    
engine = sqlalchemy.create_engine(DATABASE_URI)
session = Session(engine)

try:
    user_details = session.query(User).filter_by(useremail = user_email).first()
    fleet_details = Fleet(fleetname = fleet_name, createdby = user_details.userid)
    session.add(fleet_details)


    new_userfleet = UserFleet(user = user_details,fleet = fleet_details,
                                caninviteusers=True,
                                caninvitedevices = True,
                                canremovedevices = True,
                                canremoveusers = True)

    session.add(new_userfleet)
    session.commit()


    user_fleet_schema = UserFleetSchema()
    return user_fleet_schema.dump(new_userfleet)


except SQLAlchemyError as e:
    errorinfo = e.orig.args
    res = {}
    res['errormessage'] = e.orig.args[0]
    #if flagaws!=True:
        #   res['errormessage']+='| generated from local env setup'
    return res


session.close()


return {
    "statusCode": "200",
    "body": '{ "message": "Testing Successful" }'
}