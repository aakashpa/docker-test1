FROM python:3

COPY requirements.txt ./

RUN pip3 install -r requirements.txt

RUN mkdir /db

COPY . .

RUN [ "python", "src/Database.py" ]

CMD ["/bin/bash", "-l"]

EXPOSE 3306